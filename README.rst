========
lw-agent
========
Log watcher agent

Configuration Options
===========

The lw-agent configuration file uses [YAML](http://yaml.org/) for its syntax.

The lw-agent section specifies a list of prospectors that Filebeat uses to locate and process log files. Each prospector item begins with a dash (-) and specifies prospector-specific configuration options, including the list of paths that are crawled to locate log files.

Here is a sample configuration:

```
lw_agent:
  host: "104.155.85.212"
  port: 9091
  admin_emails:
    - dpetrov@cyl.me
  prospectors:
    -
      paths:
        - "/tmp/logs/*"
      input_type: log
      fields:
        - level: debug
        - review: 1
      document_type: application_log
      service_name: test
```

#### Options

- paths
A list of glob-based paths that should be crawled and fetched. Filebeat starts a harvester for each file that it finds under the specified paths. You can specify one path per line. Each line begins with a dash (-).

- input_type
One of the following input types:

 * log: Reads every line of the log file (default)
 * stdin: Reads the standard in
The value that you specify here is used as the input_type for each event published to CS.

- fields
Optional fields that you can specify to add additional information to the output. For example, you might add fields that you can use for filtering log data.

- document_type
The event type to use for published lines read by harvesters.





Note
====

This project has been set up using PyScaffold 2.4.2. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.
