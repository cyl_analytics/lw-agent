#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import os
import time
import ujson
import tempfile
import pytest
import multiprocessing
from toolz import concat
from lw_agent.main import run_agent
from lw_agent.prospector import split_path_to_dir_pattern

from logger import Logger
from logger.ttypes import LogMessage

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

import logging


class NSFormatter(logging.Formatter):
    def __init__(self):
        logging.Formatter.__init__(self)

    def format(self, record):
        return ujson.dumps({
            "time": int(time.time()),
            "severity": record.levelname,
            "logMessage": record.msg,
            "sourceLocation": {
                "file": record.filename,
                "line": record.lineno,
                "functionName": record.funcName
            }
        })


class LoggerHandler(object):
    def __init__(self, count):
        self.count = count
        self.logStore = []

    def log(self, logMessage):
        self.count.value += 1


def server(count):
    handler = LoggerHandler(count)
    processor = Logger.Processor(handler)
    transport = TSocket.TServerSocket(port=9090)
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()

    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)

    server.serve()


def create_config(service_name, document_type, paths):
    return {
        "lw_agent": {
            "host": "localhost",
            "port": 9090,
            "admin_emails": ["dpetrov@cyl.me"],
            "prospectors": [
                {
                    "paths": list(map(lambda p: p + "/.*\.log", paths)),
                    "input_type": "log",
                    "fields": [
                        {"level": "debug"}, {"review": 1}
                    ],
                    "document_type": document_type,
                    "service_name": service_name
                }
            ]
        }
    }


@pytest.fixture(scope='module')
def prospector(request):
    count = multiprocessing.Value('i', 0)
    s = multiprocessing.Process(target=server, args=(count, ))
    s.start()
    time.sleep(2)
    tempLogDir1 = tempfile.mkdtemp()
    tempLogDir2 = tempfile.mkdtemp()
    tempLogDir3 = tempfile.mkdtemp()
    config = create_config("test", "application_log", [tempLogDir1, tempLogDir2
                                                       ])
    config2 = create_config("test", "nginx", [tempLogDir3])
    config["lw_agent"]["prospectors"].append(
        config2["lw_agent"]["prospectors"][0])

    logging.basicConfig()

    a = multiprocessing.Process(target=run_agent, args=(logging, config))
    a.start()

    def finalize():
        print("finalize")
        s.terminate()
        a.terminate()

    request.addfinalizer(finalize)
    return (tempLogDir1, tempLogDir2, tempLogDir3, count)


@pytest.fixture(scope='module')
def filenames(prospector):
    return concat([[os.path.join(p, "test{}.log".format(i)) for i in range(
        100, 101)] for p in [prospector[0], prospector[1], prospector[2]]])


def head(ite):
    for i in ite:
        return i


def test_add_modify(prospector, filenames):
    import logging
    import logging.handlers

    log_file = head(filenames)
    time.sleep(10)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    ch = logging.handlers.RotatingFileHandler(log_file,
                                              encoding="utf-8",
                                              maxBytes=50)
    ch.setLevel(logging.INFO)
    formatter = NSFormatter()
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    n = 100
    for _ in range(n):
        time.sleep(0.2)
        logger.info({"curtime": str(int(time.time()))})
    time.sleep(20)
    assert prospector[3].value == n


def test_rotating_backup_logging(prospector, filenames):
    import logging
    import logging.handlers

    s = prospector[3].value
    log_file = head(filenames)
    time.sleep(10)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    ch = logging.handlers.RotatingFileHandler(log_file,
                                              encoding="utf-8",
                                              maxBytes=30,
                                              backupCount=5)
    ch.setLevel(logging.INFO)
    formatter = NSFormatter()
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    n = 100
    for _ in range(n):
        time.sleep(0.5)
        logger.info({"curtime": str(int(time.time()))})
    time.sleep(20)
    assert prospector[3].value - s == n


