#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import re
from yaml import safe_load, YAMLError
# In order to use LibYAML based parser and emitter
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def loadConfig(path_to_config="/etc/lw-agent/lw-agent.yml"):
    """
    Load configuration options

    Args:
      path_to_config :: str
    Return:
      Option[Dict]
    """
    try:
        config = safe_load(file(path_to_config, 'r'))
    except YAMLError, exc:
        #TODO: replace print with logger
        print("Error in configuration file:", exc)
        raise (YAMLError(exc))
    validator = []
    if "lw_agent" not in config:
        validator.append("lw_agent")
    if "host" not in config.get("lw_agent", {}):
        validator.append("host")
    if "port" not in config.get("lw_agent", {}):
        validator.append("port")
    for i, prospector in enumerate(config.get("lw_agent", {}).get(
        "prospectors", [])):
        for key in ["service_name", "document_type"]:
            if key not in prospector:
                validator.append("prospector_{}:{}".format(i, key))
        for pat in prospector["paths"]:
            try:
                re.compile(pat)
            except re.error:
                validator.append(
                    "{} is wrong python regular expression!".format(pat))
    if validator:
        error_string = '\n'.join(map(lambda k: "{key} is required".format(k),
                                     validator))
        print(error_string)
        raise (YAMLError(error_string))
    return config
