#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
import dill as pickle
import argparse
import logging
import logging.handlers
import lw_agent.config
from toolz import concat
from lw_agent.prospector import PathWatcher, split_path_to_dir_pattern
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler


def run_agent(logger, config):
    observer = Observer()
    logger.debug("Create observer")
    prospectors = []
    host = config["lw_agent"]["host"]
    port = config["lw_agent"]["port"]
    admin_emails = config["lw_agent"]["admin_emails"]

    _, hostname, _, _, _ = os.uname()

    with open("/var/lib/lw-agent/watched_files.pickle", 'r') as source:
        try:
            persistent = pickle.load(source)
        except EOFError:
            persistent = {}

    pws = {}
    for prospector in config["lw_agent"]["prospectors"]:
        if prospector["fields"] is not None:
            prospector["fields"] = dict(concat(map(
                lambda kv: map(lambda x: (str(x[0]), str(x[1])), kv.items()),
                prospector["fields"])))
        for path, pattern in map(split_path_to_dir_pattern,
                                 prospector["paths"]):
            persistent_watched_files = {}
            if path in persistent and pattern in persistent[path]:
                persistent_watched_files = persistent[path][pattern]
            pw = PathWatcher(logger, path, pattern, persistent_watched_files,
                             host, port, hostname, admin_emails,
                             prospector["service_name"],
                             prospector["document_type"],
                             prospector["input_type"], prospector["fields"])
            observer.schedule(pw, path, recursive=True)
            pws[path] = {}
            pws[path][pattern] = pw
            logger.info("Create PathWatcher {} with pattern {}".format(
                path, pattern))

    observer.start()
    try:
        while True:
            time.sleep(60 * 15)
            for path in pws.keys():
                if path not in persistent:
                    persistent[path] = {}
                for pattern in pws[path].keys():
                    persistent[path][
                        pattern
                    ] = pws[path][pattern].persist_watched_files()
            with open("/var/lib/lw-agent/watched_files.pickle", 'w') as sink:
                pickle.dump(persistent, sink)
            logger.info("save state: {}".format(repr(persistent)))
    except KeyboardInterrupt:
        map(lambda pw: map(lambda pt: pws[pw][pt].__del__(), pws[pw]), pws)
        observer.stop()
    print("wasted")


def main():
    logger = logging.getLogger(__name__)

    parser = argparse.ArgumentParser()
    parser.add_argument("-c",
                        "--conf",
                        help="path to config file",
                        default="/etc/defaults/lw-agent.yml")
    parser.add_argument("-ct",
                        "--conftest",
                        help="check config file",
                        action="store_true")
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Switch on debug mode. Log actions in STDOUT")
    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        logger.addHandler(ch)
    else:
        logger.setLevel(logging.INFO)
        ch = logging.handlers.RotatingFileHandler(
            "/var/log/lw-agent/lw-agent.log",
            maxBytes=1 << 30)
        ch.setLevel(logging.INFO)
        logger.addHandler(ch)

    if args.conftest:
        try:
            lw_agent.config.loadConfig(args.conf)
        except Exception as e:
            print(e)
            return
        print("All ok.")
    else:
        config = lw_agent.config.loadConfig(args.conf)
        run_agent(logger, config)
