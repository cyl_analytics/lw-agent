#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import time
import ujson
import requests
from toolz import concat
from pathtools.path import list_files
from pathtools.patterns import filter_paths
from watchdog.events import RegexMatchingEventHandler

import thriftpy
from thriftpy.rpc import client_context


class PathWatcher(RegexMatchingEventHandler):
    """
    Watch for every files which satisfy pattern
    and when file modified read from it
    """

    def __init__(self, logger, path, pattern, persistent_watched_files,
                 server_host, server_port, hostname, admin_emails,
                 service_name, document_type, input_type, fields):
        """
        Initialize object

        Args:
          path :: str
          pattern :: RegExp
          server_host :: str
          server_port :: int
        Return:
          PathWatcher
        """
        self.server_host = server_host
        self.server_port = server_port

        self.debug_logger = logger
        self.error_line = " Service name: " + str(
            service_name) + "\n Hostname: " + hostname + "\n Error: {}"
        self.admin_emails = admin_emails
        self.service_name = service_name
        self.document_type = document_type
        self.input_type = input_type
        self.fields = fields
        self.hostname = hostname
        self.pattern = pattern
        super(PathWatcher, self).__init__(regexes=[pattern])
        self.__watchedFiles = {}
        for filename in patternSatisfyFiles(path, pattern):
            if filename not in persistent_watched_files:
                self.__watchedFiles[filename] = open(filename, 'r')
            else:
                self.__watchedFiles[filename
                                    ] = persistent_watched_files[filename]
        self.debug_logger.info("List of watched files: {}".format(
            self.__watchedFiles.keys()))

        self.logger_thrift = thriftpy.load("/var/lib/lw-agent/logger.thrift",
                                           module_name="logger_thrift")

    def persist_watched_files(self):
        return self.__watchedFiles

    def __del__(self):
        for filename in self.__watchedFiles:
            self.__watchedFiles[filename].close()

    def on_moved(self, event):
        """
        When file moved and destination path don't satisfy pattern
        we delete file from watchedFile and if satisfy we reopen file

        Args:
          event :: watchdog.events.FileSystemMovedEvent
        Return:
          None
        """
        self.debug_logger.info("{} moved to {}".format(event.src_path,
                                                       event.dest_path))
        self.debug_logger.info("event is directory {}".format(
            event.is_directory))
        if not event.is_directory:
            self.debug_logger.info("pattern is match not none {}".format(
                re.match(self.pattern, event.dest_path) is not None))
            if re.match(self.pattern, event.dest_path) is not None:
                self.delete(event.src_path)
                self.add(event.dest_path)
            else:
                super(PathWatcher, self).on_moved(event)

    def on_created(self, event):
        """
        Add file in watchedFiles

        Args:
          event :: watchdog.events.FileSystemEvent
        Return:
          None
        """
        self.debug_logger.info("created {}".format(event.src_path))

        if not event.is_directory:
            self.add(event.src_path)
            self.modified(event.src_path)

    def on_deleted(self, event):
        """
        Delete file from watchedFiles

        Args:
          event :: watchdog.events.FileSystemEvent
        Return:
          None
        """
        self.debug_logger.info("deleted {}".format(event.src_path))

        if not event.is_directory:
            self.delete(event.src_path)

    def on_modified(self, event):
        """
        Read from modified file

        Args:
          event :: watchdog.events.FileSystemEvent
        Return:
          None
        """
        if not event.is_directory:
            self.modified(event.src_path)

    def add(self, key):
        self.debug_logger.info("add function")
        if key not in self.__watchedFiles:
            self.debug_logger.info("add {}".format(key))
            self.__watchedFiles[key] = open(key, 'rb')
        else:
            self.delete(key)
            self.add(key)

    def delete(self, key):
        self.debug_logger.info("delete function")
        if key in self.__watchedFiles:
            self.debug_logger.info("delete {}".format(key))
            self.__watchedFiles[key].close()
            self.__watchedFiles.pop(key)

    def modified(self, key):
        try:
            with client_context(self.logger_thrift.Logger, self.server_host,
                                self.server_port) as logger:
                while True:
                    line = self.__watchedFiles[key].readline()
                    if not line:
                        break
                    try:
                        jmessage = ujson.loads(line)
                    except Exception as e:
                        notify_admin(self.admin_emails,
                                     self.error_line.format(e))
                        continue

                    logMessage = self.logger_thrift.LogMessage()
                    logMessage.service_name = self.service_name
                    logMessage.document_type = self.document_type
                    logMessage.input_type = self.input_type
                    logMessage.source = key

                    message = self.logger_thrift.LogLine()
                    try:
                        message.time = str(jmessage["time"])
                        if "severity" in jmessage:
                            message.severity = self.logger_thrift.LogSeverity(
                            )._NAMES_TO_VALUES[jmessage["severity"]]
                        message.logMessage = ujson.dumps(jmessage["logMessage"
                                                                  ])

                    except Exception as e:
                        self.debug_logger.exception(repr(e))
                        notify_admin(self.admin_emails, self.error_line.format(
                            "error {} during parsing jmessage: {}\nSource: {}; Line: {}".format(
                                e, jmessage, key, line)))
                        continue
                    logMessage.message = message
                    logMessage.hostname = self.hostname
                    if self.fields is not None:
                        logMessage.fields = self.fields
                    try:
                        logger.log(logMessage)
                    except Exception as e:
                        self.debug_logger.exception(repr(e))
                        break

                    self.debug_logger.debug("Send log line: {}".format(
                        logMessage))
        except Exception as e:
            self.debug_logger.exception(repr(e))
            time.sleep(10)
            self.modified(key)


def split_path_to_dir_pattern(path):
    """
    Split given path to directory and pattern
    directory can not contain wildcards

    Args:
      path :: str
    return:
      Tuple[str, str]
    """
    if path and path[0] != '/':
        path = os.path.join(os.getcwd(), path)

    dir = ""
    path_temp = "/"
    for pname in path.split('/'):
        path_temp = os.path.join(path_temp, pname.replace("\\", ''))
        if os.path.isdir(path_temp):
            dir = path_temp
        else:
            break

    if dir == path:
        pattern = os.path.join(path, "*")
    else:
        pattern = path
    return (dir, pattern)


def takeWhile(guard, l):
    """
    Take element while guard is True

    Args:
      guard :: a -> Bool
      l :: Iterable[a]
    Return:
      Iterable[a]
    """
    for el in l:
        if guard(el):
            yield el
        else:
            raise StopIteration


def patternSatisfyFiles(path, pattern):
    """
    Return all files in path which satisfy pattern

    Args:
      path :: str
      pattern :: RegExp
    Return:
      [str]
    """
    _match = re.compile(pattern).match
    for filename in list_files(path):
        if _match(filename) is not None:
            yield filename


def notify_admin(admin_emails, msg_body):
    """
    send email to admin
    """
    return requests.post(
        "https://api.mailgun.net/v3/sandboxb2c5ca9b5dc74911a8505d6dac78b4f9.mailgun.org/messages",
        auth=("api", "key-d43dee132576aaba756dbe38dafd28cc"),
        data={
            "from":
            "Excited User <mailgun@sandboxb2c5ca9b5dc74911a8505d6dac78b4f9.mailgun.org>",
            "to": admin_emails,
            "subject": "Error lw-agent",
            "text": msg_body
        })
