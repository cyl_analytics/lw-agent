
enum LogSeverity {
     DEFAULT,
     DEBUG,
     INFO,
     NOTICE,
     WARNING,
     ERROR,
     CRITICAL,
     ALERT,
     EMERGENCY
}

struct LogLine {
       1: string time;
       2: optional LogSeverity severity=LogSeverity.INFO;
       3: string logMessage;
}

struct LogMessage {
  1: string service_name;
  2: string document_type;
  3: string input_type="log";
  4: string source;
  5: LogLine message;
  6: string hostname
  16: optional map<string,string> fields
}


service Logger {
  oneway void log(1:LogMessage logMessage)
}